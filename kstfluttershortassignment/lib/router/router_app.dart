import 'package:get/get.dart';
import 'package:kstfluttershortassignment/modules/binding/favorite_binding.dart';
import 'package:kstfluttershortassignment/modules/binding/main_binding.dart';
import 'package:kstfluttershortassignment/modules/view/dashboard.dart';
import 'package:kstfluttershortassignment/modules/view/favorite.dart';
import 'package:kstfluttershortassignment/modules/view/home.dart';
import 'package:kstfluttershortassignment/router/router_page.dart';

class AppPages {
  static final pages = [
    GetPage(
      name: DashboardViewRoute, 
      page: () => DashboardView(),
      binding: MainBinding()
    ),
    GetPage(
      name: HomeViewRoute, 
      page: () => const HomeView(),
      binding: MainBinding()
    ),
    GetPage(
      name: FavoriteViewRoute, 
      page: () => const FavoriteView(),
      binding: FavoriteBinding()
    )
  ];
}