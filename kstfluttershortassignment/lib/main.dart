import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:kstfluttershortassignment/router/router_app.dart';
import 'package:kstfluttershortassignment/router/router_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations(
      [
        DeviceOrientation.portraitUp,
        DeviceOrientation.portraitDown
      ]
    );
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'KST Flutter Short Assignment',
      getPages: AppPages.pages,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: DashboardViewRoute,
    );
  }
}