import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kstfluttershortassignment/core/values/colors.dart';
import 'package:kstfluttershortassignment/core/values/font.dart';
import 'package:kstfluttershortassignment/modules/controller/main_controller.dart';

class MovieCard extends StatefulWidget {
  final String rank;
  final String imageUrl;
  final String title;
  final String year;
  bool isFavorite;
  MovieCard({ Key? key, 
    required this.rank, 
    required this.imageUrl, 
    required this.title, 
    required this.year,
    required this.isFavorite}) : super(key: key);

  @override
  State<MovieCard> createState() => _MovieCardState();
}

class _MovieCardState extends State<MovieCard> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: const EdgeInsets.all(8),
          margin: const EdgeInsets.only(top: 8, bottom: 8),
          height: Get.mediaQuery.size.height / 6,
          width: Get.mediaQuery.size.width,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Expanded(
                  flex: 1,
                  child: Container(
                    margin: const EdgeInsets.only(right: 10),
                    child: Text(
                      widget.rank,
                      textAlign: TextAlign.center,
                      style: h6(color: contextRed),
                    ),
                  )),
              Expanded(
                  flex: 2,
                  child: CachedNetworkImage(
                    imageUrl: widget.imageUrl,
                    width: Get.mediaQuery.size.width / 6,
                    height: Get.mediaQuery.size.height / 2,
                    fadeInDuration: const Duration(microseconds: 300),
                    errorWidget: (context, url, error) =>
                        const Icon(Icons.error),
                  )),
              Expanded(
                  flex: 6,
                  child: Container(
                    margin: const EdgeInsets.only(left: 8),
                    child: Text(
                      '${widget.title} (${widget.year})',
                      textAlign: TextAlign.center,
                      style: h6(color: contextRed),
                    ),
                  )),
              Expanded(
                flex: 1,
                child: InkWell(
                      onTap: () {
                        setState(() {
                          widget.isFavorite = !widget.isFavorite;
                        });
                      },
                      child: Icon(
                        widget.isFavorite == false
                            ? Icons.favorite_border_outlined
                            : Icons.favorite,
                        color: contextRed,
                      ),
                    ),
              )
            ],
          ),
        ),
        const Divider(
          thickness: 1,
          color: contextRed,
        )
      ],
    );
  }
}