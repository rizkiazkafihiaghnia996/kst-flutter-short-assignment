import 'package:kstfluttershortassignment/data/api/api_provider.dart';
import 'package:kstfluttershortassignment/data/model/movies_response.dart';

class Repository {
  ApiProvider _apiProvider = ApiProvider();

  Future<MoviesResponse> getMovies() async {
    final response = await _apiProvider.getMovies();
    return MoviesResponse.fromJson(response);
  }
}