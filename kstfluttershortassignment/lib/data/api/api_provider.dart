import 'package:dio/dio.dart';
import 'package:kstfluttershortassignment/data/model/movies_response.dart';
import 'package:kstfluttershortassignment/data/provider/db_provider.dart';

class ApiProvider {
  Dio dio = Dio();
  final String apiKey = 'k_c8xxue3y';
  final String baselineUrl = 'https://imdb-api.com/en/API/';

  getResponse(statusCode, error, message, data) {
    return {
      'status_code': statusCode,
      'error': error,
      'message': message,
      'data': data
    };
  }

  handleError(error) {
    String errorDescription = '';
    DioError? dioError;

    if (error is DioError) {
      dioError = error as DioError;
      switch(dioError.type) {
        case DioErrorType.cancel:
          errorDescription = "Request to the server was cancelled";
          break;
        case DioErrorType.connectTimeout:
          errorDescription = "Connection timeout with the server";
          break;
        case DioErrorType.other:
          errorDescription = "Unknown error occured";
          break;
        case DioErrorType.receiveTimeout:
          errorDescription = "Receive Timeout in connection with the server";
          break;
        case DioErrorType.sendTimeout:
          errorDescription = "Send Timeout in connection with the server";
          break;
        case DioErrorType.response:
          errorDescription = "Something went wrong";
          break;
      }
    } else {
      errorDescription = "Unexpected error occurred";
    }

    return getResponse(
      dioError!.response!.statusCode, 
      true, 
      errorDescription, 
      dioError.response!.data['error_data']
    );
  }

  Future dioGet(url) async {
    try {
      final response = await dio.get(baselineUrl + url,
        options: Options(
          headers: {
            'Content-Type': 'application/json',
            'Charset': 'utf-8'
          }
        )
      );
      
      return response.data;   
    } catch(e) {
      handleError(e);
    }
  }

  Future getMovies() async {
    return await dioGet('MostPopularMovies/$apiKey');
  }
}