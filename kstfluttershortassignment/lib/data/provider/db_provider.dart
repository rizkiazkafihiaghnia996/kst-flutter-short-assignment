/*import 'dart:io';

import 'package:kstfluttershortassignment/data/model/movies_response.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DBProvider {
  static Database? _database;
  static final DBProvider db = DBProvider._();
  
  DBProvider._();

  Future<Database?> get database async {
    if(_database != null) return _database!;
 
    _database = await initDB();

    return _database;
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    final path = join(documentsDirectory.path, 'movies_list.db');

    return await openDatabase(path, version: 1, onOpen: (db) {}, 
        onCreate: (Database db, int version) async {
      await db.execute('CREATE TABLE Movies('
          'id TEXT PRIMARY KEY,'
          'rank TEXT,'
          'title TEXT,'
          'year TEXT,'
          'image TEXT'
      ')');
    });
  }

  Future<MoviesResponse> createMovies(MoviesResponse movies) async {
    await deleteAllMovies();
    final db = await database;

    final json = movies.toJson();
    if (json['items'] != null) {
      var batch = db!.batch();

      json['items'].forEach((movies) {
        batch.insert('Movies', movies);
      });

      await batch.commit();
    }
    print(json['items']);
    print('Movies are stored in the database');

    return movies;
  }

  Future<int> deleteAllMovies() async {
    final db = await database;
    final res = await db!.rawDelete('DELETE FROM Movies');

    return res;
  }

  Future<List<MoviesResponse>> getAllMovies() async {
    final db = await database;
    final res = await db!.rawQuery("SELECT * FROM Movies");

    List<MoviesResponse> list = 
        res.isNotEmpty ? res.map((e) => MoviesResponse.fromJson(e))
        .toList() : [];

    return list;
  }
}*/