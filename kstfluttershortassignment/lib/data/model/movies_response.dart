import 'dart:convert';

MoviesResponse moviesResponseFromJson(String str) => MoviesResponse.fromJson(json.decode(str));

String moviesResponseToJson(MoviesResponse data) => json.encode(data.toJson());

class MoviesResponse {
    MoviesResponse({
        required this.items,
        required this.errorMessage
    });

    List<Item> items;
    String errorMessage;

    factory MoviesResponse.fromJson(Map<String, dynamic> json) => MoviesResponse(
        items: List<Item>.from(json["items"].map((x) => Item.fromJson(x))),
        errorMessage: json["errorMessage"],
    );

    Map<String, dynamic> toJson() => {
        "items": List<dynamic>.from(items.map((x) => x.toJson())),
        "errorMessage": errorMessage,
    };
}

class Item {
    Item({
       required this.id,
       required this.rank,
       required this.rankUpDown,
       required this.title,
       required this.fullTitle,
       required this.year,
       required this.image,
       required this.crew,
       required this.imDbRating,
       required this.imDbRatingCount,
       this.isFavorite = false
    });

    String id;
    String rank;
    String rankUpDown;
    String title;
    String fullTitle;
    String year;
    String image;
    String crew;
    String imDbRating;
    String imDbRatingCount;
    bool isFavorite;

    factory Item.fromJson(Map<String, dynamic> json) => Item(
        id: json["id"],
        rank: json["rank"],
        rankUpDown: json["rankUpDown"],
        title: json["title"],
        fullTitle: json["fullTitle"],
        year: json["year"],
        image: json["image"],
        crew: json["crew"],
        imDbRating: json["imDbRating"],
        imDbRatingCount: json["imDbRatingCount"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "rank": rank,
        "rankUpDown": rankUpDown,
        "title": title,
        "fullTitle": fullTitle,
        "year": year,
        "image": image,
        "crew": crew,
        "imDbRating": imDbRating,
        "imDbRatingCount": imDbRatingCount,
    };
}
