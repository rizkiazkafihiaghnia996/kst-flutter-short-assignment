import 'package:get/get.dart';
import 'package:kstfluttershortassignment/data/api/api_provider.dart';
import 'package:kstfluttershortassignment/data/api/repository.dart';
import 'package:kstfluttershortassignment/data/model/movies_response.dart';
import 'package:kstfluttershortassignment/data/provider/db_provider.dart';

class MainController extends GetxController {
  Repository _repository = Repository();

  @override
  void onInit() {
    getMovies();
    super.onInit();
  }

  RxInt _selectedPageIndex = 0.obs;
  RxBool _movieLoading = false.obs;
  Rxn<MoviesResponse> _moviesData = Rxn<MoviesResponse>();
  RxBool _isFavorite = false.obs;

  int get selectedPageIndex => _selectedPageIndex.value;
  bool get movieLoading => this._movieLoading.value;
  MoviesResponse? get moviesData => this._moviesData.value;
  bool get isFavorite => this._isFavorite.value;

  set updateSelectedPageIndex(int selectedPageIndex) =>
      this._selectedPageIndex.value = selectedPageIndex;
  set movieLoading(bool movieLoading) =>
      this._movieLoading.value = movieLoading;
  set moviesData(MoviesResponse? moviesData) =>
      this._moviesData.value = moviesData;
  set isFavorite(bool isFavorite) =>
      this._isFavorite.value = isFavorite;

  void onNavBarItemTapped(int index) {
    updateSelectedPageIndex = index;
  }
  
  Future<MoviesResponse?> getMovies() async {
    movieLoading = true;
    MoviesResponse res = await _repository.getMovies();
    moviesData = res;
    movieLoading = false;

    return moviesData;
  }

  /*deleteMovies() async {
    movieLoading = true;
    await DBProvider.db.deleteAllMovies();
    movieLoading = false;
    print('All movies have been deleted');
  }*/
}