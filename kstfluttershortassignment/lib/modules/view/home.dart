import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kstfluttershortassignment/core/values/colors.dart';
import 'package:kstfluttershortassignment/core/values/font.dart';
import 'package:kstfluttershortassignment/data/model/movies_response.dart';
import 'package:kstfluttershortassignment/global_widget/movie_card.dart';
import 'package:kstfluttershortassignment/modules/controller/main_controller.dart';

class HomeView extends GetView<MainController> {
  const HomeView({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    MainController controller = Get.find<MainController>();

    return Scaffold(
      backgroundColor: Colors.black,
      body: RefreshIndicator(
        onRefresh: () {
          return controller.getMovies();
        },
        child: Obx(() {
          if (controller.movieLoading == true) {
            return const SafeArea(
              child: Center(
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(contextRed),
                ),
              )
            );
          } else {
            MoviesResponse? data = controller.moviesData;
            return SafeArea(
              child: Container(
                height: Get.mediaQuery.size.height,
                width: Get.mediaQuery.size.width,
                padding: const EdgeInsets.all(8),
                child: SingleChildScrollView(
                  physics: const BouncingScrollPhysics(),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Most Popular Movies',
                        style: h1(color: contextRed),
                      ),
                      const SizedBox(height: 4),
                      Text(
                        'The Latest (Sometimes Classic) and Greatest Blockbuster Movies',
                        style: h5(
                          fontWeight: FontWeight.normal,
                          color: Colors.white
                        ),
                      ),
                      const SizedBox(height: 32),
                      Column(
                        children: [
                          Row(
                            children: [
                              Expanded(
                                flex: 1,
                                child: Container(
                                  margin: const EdgeInsets.only(left: 0),
                                  child: Text(
                                    'Rank',
                                    textAlign: TextAlign.center,
                                    style: h6(color: contextRed),
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 2,
                                child: Container(
                                  margin: const EdgeInsets.only(right: 6),
                                  child: Text(
                                    'Poster',
                                    textAlign: TextAlign.center,
                                    style: h6(color: contextRed),
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 6,
                                child: Container(
                                  margin: const EdgeInsets.only(right: 40),
                                  child: Text(
                                    'Title and Year',
                                    textAlign: TextAlign.center,
                                    style: h6(color: contextRed),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          const Divider(
                            thickness: 2,
                            color: contextRed,
                          )
                        ],
                      ),
                      Builder(
                        builder: (context) {
                          return Column(
                            children: List.generate(
                              controller.moviesData!.items.length,
                              (index) => MovieCard(
                                rank: data!.items[index].rank, 
                                imageUrl: data.items[index].image, 
                                title: data.items[index].title, 
                                year: data.items[index].year,
                                isFavorite: data.items[index].isFavorite,
                              )
                            ),
                          );
                        }
                      )
                    ],
                  )
                ),
              )
            );
          }
        } 
          ),
      ));
  }
}