import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kstfluttershortassignment/core/values/colors.dart';
import 'package:kstfluttershortassignment/core/values/font.dart';
import 'package:kstfluttershortassignment/modules/controller/main_controller.dart';
import 'package:kstfluttershortassignment/modules/view/favorite.dart';
import 'package:kstfluttershortassignment/modules/view/home.dart';

class DashboardView extends GetView<MainController> {
  MainController controller = Get.find<MainController>();

  final List<Widget> pages = [
    const HomeView(
      key: PageStorageKey('Page1'),
    ),
    const FavoriteView(
      key: PageStorageKey('Page2'),
    )
  ];

  final List<BottomNavigationBarItem> items = [
    BottomNavigationBarItem(
      activeIcon: Container(
        padding: const EdgeInsets.all(6),
        margin: const EdgeInsets.only(left: 20, right: 12),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50),
          color: contextRed,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Icon(
              Icons.home_rounded,
              size: 35,
            ),
            SizedBox(width: 8),
            Text(
              "Home",
              style: h5(),
            )
          ],
        )
      ),
      label: "",
      icon: const Icon(
        Icons.home_outlined,
        size: 35,
      )
    ),
    BottomNavigationBarItem(
      activeIcon: Container(
        padding: const EdgeInsets.all(6),
        margin: const EdgeInsets.only(left: 20, right: 12),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50),
          color: contextRed,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Icon(
              Icons.favorite,
              size: 35,
            ),
            SizedBox(width: 8),
            Text(
              "Favorite",
              style: h5(),
            )
          ],
        )
      ),
      label: "",
      icon: const Icon(
        Icons.favorite_border_outlined,
        size: 35,
      )
    ),
  ];

  final PageStorageBucket bucket = PageStorageBucket();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Obx(() => PageStorage(
        bucket: bucket, 
        child: pages[controller.selectedPageIndex]
      )),
      bottomNavigationBar: Container(
        decoration: const BoxDecoration(
          color: Colors.black,
          border: Border(
            top: BorderSide(
              width: 2,
              color: contextRed
            )
          )
        ),
        child: Obx(() => BottomNavigationBar(
          items: items,
          backgroundColor: Colors.black,
          selectedItemColor: Colors.black,
          unselectedItemColor: contextRed,
          currentIndex: controller.selectedPageIndex,
          onTap: controller.onNavBarItemTapped
        )),
      ),
    );
  }
}