import 'package:get/get.dart';
import 'package:kstfluttershortassignment/modules/controller/main_controller.dart';

class MainBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<MainController>(() => MainController());
  }
}