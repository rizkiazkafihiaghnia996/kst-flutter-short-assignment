import 'dart:ffi';

import 'package:get/get.dart';

class FavoriteBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<FavoriteBinding>(() => FavoriteBinding());
  }
}